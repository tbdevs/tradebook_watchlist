package com.tradebook.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.tradebook.model.Stock;

@Repository
public interface StockRepository extends MongoRepository<Stock, String> {

	List findStockByName(String name);

}
